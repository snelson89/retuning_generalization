# Lexically-guided perceptual learning does generalize to new phonetic contexts

## Overview

This repository contains the data and analysis scripts for the experiments in Nelson & Durvasula (submitted). If you
make use of the materials in this repository, please cite our paper:

- Nelson, Scott & Karthik Durvasula. 2020. Lexically-guided perceptual learning does generalize to new phonetic contexts. Ms. Stony Brook University.

Here is a BibTeX entry:

``` text
@unpublished{nelson2020,
  Author = {Nelson, Scott and Durvasula, Karthik},
  Title = {Lexically-guided perceptual learning does generalize to new phonetic contexts},
  Year = {2020}}
```

## Data

### Experiment 1

There is a CSV file for each participant in `Data/Experiment1`. The data is further divided into the subdirectories
`Control` and `Test`. In the paper we report testing 94 English speaking participants. We additionally tested several
non-native English speakers due to the way in which some of the data were collected (as extra credit for a class). The
data files for these participants (participants 76, 78-80) are not included in this public repository since they
were never going to be included in the analysis. 

We additionally excluded certain English speaking participants data from our analysis due to low performance in the
lexical decision task. The data for those participants is included in this public repository.


### Experiment 2

There is a CSV file for each participant in `Data/Experiment2`. The data is further divided into the subdirectories
`Control` and `Test`. In the paper we report testing 131 English speaking participants. We additionally tested several
non-native English speakers due to the way in which some of the data were collected (as extra credit for a class). The
data files for these participants (participants 7-8, 15, 44-45, 54, 57, 70, 74, 76, 114-115, 119-121, 136, 138, 149, 151) 
are not included in this public repository since they were never going to be included in the analysis. 

We additionally excluded certain English speaking participants data from our analysis due to low performance in the
lexical decision task. The data for those participants is included in this public repository.



### Experiment 3

There is a CSV file for each participant in `Data/Experiment3`. The data is further divided into the subdirectories
`Control` and `Test`. In the paper we report testing 120 English speaking participants. We additionally tested several
non-native English speakers due to the way in which some of the data were collected (as extra credit for a class). The
data files for these participants (participants 15, 40, 43, 50-52, 54, 58, 74, 76, 132) are not included in this 
public repository since they were never going to be included in the analysis. 

We additionally excluded certain English speaking participants data from our analysis due to low performance in the
lexical decision task. The data for those participants is included in this public repository.



## Analysis script

There is an analysis script in the `Analysis` directory. This script reproduces all of the analyses and figures
reported in the paper. 

In order to run the script properly, please provide path arguments for each specified directory on lines 15-20. 
The plots for each experiment will be saved to your working directory. 

Though not fully reported in the paper, we mention in footnotes 13 and 15 that we also ran fully bayesian logistic mixed
effect models for Experiments 2-3. The code for this analysis is provided in lines 381-445.


