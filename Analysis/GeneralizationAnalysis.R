#This script writes a function to analyse the data for the generalisation manuscript
#It analyses all the relevant experiments with the function "GeneralisationMSAnalysis" 
#glmer stats now added.

library(tidyverse)
library(gridExtra)
library(lme4)
library(lmerTest)
library(parameters)
library(brms)
library(BayesFactor)

# Adjust the following lines with paths to each directory

Exp1Test_Directory = ""
Exp1Control_Directory = ""
Exp2Test_Directory = ""
Exp2Control_Directory = ""
Exp3Test_Directory = ""
Exp3Control_Directory = ""



#**************
#Relevant Functions

#Reading in Data
#LDTAccuracyRateChoice = "?andS"
dataMunger = function(experiment=Exp1Test_Directory,
                      Language = "American English", 
                      LDTAccuracyRateChoiceOverall=FALSE, LDTthreshold = 0.50,
                      ExpType="Test"){
  
  #Getting test data
  files = list.files(path=paste0(experiment), pattern="*.csv", full.names = TRUE)
  Original = NULL
  for(fileNum in c(1:length(files))){
    data_temp=read.csv(files[fileNum]) %>% 
      mutate(SubNum=fileNum) %>% 
      filter(Native.Language %in% Language)
    
    #Fixing inconsistent coding between experiments
    if(grepl("Experiment3",experiment)){
      data_temp = data_temp %>% 
        mutate(segment = as.character(segment)) %>%
        mutate(segment = ifelse(segment=="?","f/?",segment),
               segment = ifelse(segment=="n/a","None",segment)) %>% 
        mutate(segment = as.factor(segment))
    }
    if(grepl("Experiment2",experiment)){
      data_temp = data_temp %>% 
        mutate(segment = as.character(segment)) %>%
        mutate(segment = ifelse(segment=="?","f/?",segment),
               segment = ifelse(segment=="f","f/?",segment),
               segment = ifelse(segment=="n/a","None",segment)) %>% 
        mutate(segment = as.factor(segment))
    }
    if(grepl("Experiment1",experiment) | grepl("Experiment2",experiment)){
      data_temp = data_temp %>% 
        mutate(segment = as.character(segment)) %>%
        mutate(segment = ifelse(segment=="?","f/?",segment),
               segment = ifelse(segment=="f","f/?",segment),
               segment = ifelse(segment=="n/a","None",segment)) %>% 
        mutate(segment = as.factor(segment))
    }
    
    if("mouse.x" %in% names(data_temp)){
      data_temp = data_temp %>% select(-c(mouse_5.leftButton,mouse_5.midButton,mouse_5.rightButton,mouse_5.x,
                                          mouse_5.y,mouse.leftButton,mouse.midButton,mouse.rightButton,
                                          mouse.x,mouse.y))
    }
    
    if("psychopyVersion" %in% names(data_temp)){
      data_temp = data_temp %>% select(-c(psychopyVersion))
    }
    
    if("X" %in% names(data_temp)){
      data_temp = data_temp %>% select(-c(X))
    }
    Original = rbind(Original,data_temp)
  }
  
  
  #Getting LDT data
  LDT = Original %>% 
    filter(address == "LDTSounds/") %>% 
    select(realvsnonce:type,key_resp_3.keys:key_resp_3.rt,SubNum,segment) %>% 
    # New response code to differentiate realvsnonce responses
    mutate(response = ifelse(realvsnonce=="nonce",
                             ifelse(key_resp_3.keys=="l",0,
                                    ifelse(key_resp_3.keys=="a",1,NA)),
                             ifelse(key_resp_3.keys=="l",1,
                                    ifelse(key_resp_3.keys=="a",0,NA))))
  #Getting LDT accuracy rates
  LDTAccuracySegment = LDT %>% 
    group_by(SubNum,segment) %>% 
    summarise(meanResponse=mean(response,na.rm=T)) %>%
    spread(key="segment",value=meanResponse)
  LDTAccuracyOverall = LDT %>% 
    group_by(SubNum) %>% 
    summarise(Overall=mean(response,na.rm=T))
  LDTAccuracy=inner_join(LDTAccuracySegment,LDTAccuracyOverall) %>% 
    mutate(ExpType = ExpType)
  
  #Good Participants - Either overall, or both S and ? have to be above 50%
  if(LDTAccuracyRateChoiceOverall){
    goodSubjects = LDTAccuracy %>% 
      filter(Overall > LDTthreshold)
  }else{
    goodSubjects = LDTAccuracy %>% 
      filter(s > LDTthreshold, `f/?` > LDTthreshold)
  }
  
  
  
  #Getting Categorisation data
  Categorization = Original %>% 
    filter(!is.na(ChosenStep)) %>% 
    filter(SubNum %in% goodSubjects$SubNum) %>% 
    mutate(Type = ifelse(is.na(trials.thisN),
                         ifelse(!is.na(trials_3.thisN), "After", NA),
                         "Before")) %>% 
    mutate(Response = ifelse(rating.response == "s",1,
                             ifelse(rating.response == "f",0,NA)))  %>%
    mutate(ExpType = ExpType)
  
  Final = list(LDT=LDTAccuracy,goodSubjects=goodSubjects,Categorization=Categorization)
  
  return(Final)  
}

#glmer - used in second ms
GeneralisationMSAnalysis_glmer = function(test=Exp1Test_Directory,control=Exp1Control_Directory,outputFilename="Experiment1"){
  
  analysisWindowPoints = c(15,17,19)
  
  #Reading in data
  testDataOriginal = dataMunger(experiment=test,ExpType="Test")
  controlDataOriginal = dataMunger(experiment=control,ExpType="Control")
  
  #Categorization data
  (LDT = rbind(testDataOriginal$goodSubjects,controlDataOriginal$goodSubjects) %>% 
      group_by(ExpType) %>% 
      summarise(NumSubjects = length(SubNum),
                # `?` = mean(`?`,na.rm=T),
                None = mean(None,na.rm=T),
                s = mean(s,na.rm=T),
                `f/?` = mean(`f/?`,na.rm=T),
                Overall = mean(Overall,na.rm=T)))
  
  #Categorization data
  testCategorization = testDataOriginal$Categorization
  sort(names(testCategorization))
  controlCategorization = controlDataOriginal$Categorization %>% 
    mutate(SubNum = SubNum + max(unique(testCategorization$SubNum))) #This is to make sure that the participants have different sub nums in the two groups
  setdiff(names(testCategorization),names(controlCategorization))
  
  #Concatenating the data
  Categorization = rbind(testCategorization,controlCategorization) %>% 
    select(SubNum,Type,ExpType,ChosenStep,Response) %>% 
    mutate(RowNum=row_number()) %>% 
    filter(ChosenStep %in% analysisWindowPoints) %>% 
    mutate(Type = fct_relevel(Type, "Before"))
  length(unique(Categorization$SubNum))
  
  #glmer - Comparing before vs. after - control
  #Note: (Type|ChosenStep) doesn't converge for many of the models.
  #Comparing Before-After in Control
  CategorizationBeforeAfter = Categorization %>% filter(ExpType=="Control")
  m0 = glmer(data=CategorizationBeforeAfter,Response~1 + (Type|SubNum) + (1|ChosenStep), family="binomial")
  m1 = glmer(data=CategorizationBeforeAfter,Response~Type + (Type|SubNum) + (1|ChosenStep), family="binomial")
  (anova_BeforeAfter_Control = data.frame(anova(m0,m1)))
  parameter_m0_BeforeAfter_Control = summary(m0)$coefficients
  parameter_m1_BeforeAfter_Control = summary(m1)$coefficients
  
  #Comparing Before-After in Test
  CategorizationBeforeAfter = Categorization %>% filter(ExpType=="Test")
  m0 = glmer(data=CategorizationBeforeAfter,Response~1 + (Type|SubNum) + (1|ChosenStep), family="binomial")
  m1 = glmer(data=CategorizationBeforeAfter,Response~Type + (Type|SubNum) + (1|ChosenStep), family="binomial")
  (anova_BeforeAfter_Test = data.frame(anova(m0,m1)))
  parameter_m0_BeforeAfter_Test = summary(m0)$coefficients
  parameter_m1_BeforeAfter_Test = summary(m1)$coefficients
  
  #Comparing Before-Before in Control vs. Test
  CategorizationBeforeBefore = Categorization %>% filter(Type=="Before")
  m0 = glmer(data=CategorizationBeforeBefore,Response~1 + (ExpType|SubNum) + (1|ChosenStep), family="binomial")
  m1 = glmer(data=CategorizationBeforeBefore,Response~ExpType + (ExpType|SubNum) + (1|ChosenStep), family="binomial")
  (anova_TestControl_BeforeBefore = data.frame(anova(m0,m1)))
  parameter_m0_TestControl_BeforeBefore = summary(m0)$coefficients
  parameter_m1_TestControl_BeforeBefore = summary(m1)$coefficients
  
  #Comparing After-After Control vs. Test
  CategorizationTestControl = Categorization %>% filter(Type=="After")
  m0 = glmer(data=CategorizationTestControl,Response~1 + (ExpType|SubNum) + (1|ChosenStep), family="binomial")
  m1 = glmer(data=CategorizationTestControl,Response~ExpType + (ExpType|SubNum) + (1|ChosenStep), family="binomial")
  (anova_TestControl_AfterAfter = data.frame(anova(m0,m1)))
  parameter_m0_TestControl_AfterAfter = summary(m0)$coefficients
  parameter_m1_TestControl_AfterAfter = summary(m1)$coefficients
  
  #glmer - Comparing interaction (Before vs. After) Vs. (Test vs. Control)
  m0 = glmer(data=Categorization,Response~1 + (1|SubNum) + (1|ChosenStep), family="binomial")
  m1 = glmer(data=Categorization,Response~ExpType + (1|SubNum) + (1|ChosenStep), family="binomial")
  m2 = glmer(data=Categorization,Response~Type + (1|SubNum) + (1|ChosenStep), family="binomial")
  m3 = glmer(data=Categorization,Response~ExpType + Type + (1|SubNum) + (1|ChosenStep), family="binomial")
  m4 = glmer(data=Categorization,Response~ExpType*Type + (1|SubNum) + (1|ChosenStep), family="binomial")
  (anova_Test_Full = data.frame(anova(m0,m1,m2,m3,m4)))
  parameter_m0_Full = summary(m0)$coefficients
  parameter_m1_Full = summary(m1)$coefficients
  parameter_m2_Full = summary(m2)$coefficients
  parameter_m3_Full = summary(m3)$coefficients
  parameter_m4_Full = summary(m4)$coefficients
  
  #************
  #Plots
  #Separate Experiments
  CategorizationBeforeAfter_means = rbind(testCategorization,controlCategorization) %>% 
    select(SubNum,Type,ExpType,ChosenStep,Response) %>% 
    mutate(RowNum=row_number()) %>% 
    mutate(Type = fct_relevel(Type, "Before")) %>% 
    group_by(SubNum,Type,ExpType,ChosenStep) %>% 
    summarise(meanResponse=mean(Response,na.rm=T))
  CategorizationBeforeAfterDifferenceControlvsTest_means = CategorizationBeforeAfter_means %>% 
    spread(key=Type,value=meanResponse) %>% 
    mutate(Difference=After-Before)
  
  (plotControl = CategorizationBeforeAfter_means %>%
      filter(ExpType=="Control") %>% 
      group_by(Type,ChosenStep) %>% 
      summarise(meanResponse=mean(meanResponse,na.rm=T)) %>% 
      ggplot(aes(x=ChosenStep,y=meanResponse,group=Type,colour=Type))+
      geom_rect(xmin=analysisWindowPoints[1],xmax=analysisWindowPoints[3],ymin=0.05,ymax=0.95, colour="grey60", fill="grey100", alpha=0) +
      geom_point()+
      geom_line()+
      ylab("Proportion of alveolar responses")+
      theme_classic()+
      theme(text = element_text(size=15))+
      ggtitle("Control: Before vs. After comparison"))
  
  (plotTest = CategorizationBeforeAfter_means %>% 
      filter(ExpType=="Test") %>% 
      group_by(Type,ChosenStep) %>% 
      summarise(meanResponse=mean(meanResponse,na.rm=T)) %>% 
      ggplot(aes(x=ChosenStep,y=meanResponse,group=Type,colour=Type))+
      geom_rect(xmin=analysisWindowPoints[1],xmax=analysisWindowPoints[3],ymin=0.05,ymax=0.95, colour="grey60", fill="grey100", alpha=0) +
      geom_point()+
      geom_line()+
      ylab("Proportion of alveolar responses")+
      theme_classic()+
      theme(text = element_text(size=15))+
      ggtitle("Test: Before vs. After comparison"))
  
  (plotControlTestDifferences = CategorizationBeforeAfterDifferenceControlvsTest_means %>% 
      group_by(ExpType,ChosenStep) %>% 
      summarise(meanDifference=mean(Difference,na.rm=T)) %>% 
      ggplot(aes(x=ChosenStep,y=meanDifference,group=ExpType,colour=ExpType))+
      geom_rect(xmin=analysisWindowPoints[1],xmax=analysisWindowPoints[3],ymin=-0.5,ymax=0.5, colour="grey60", fill="grey100", alpha=0) +
      geom_point()+
      geom_line()+
      ylab("Proportion of alveolar responses")+
      theme_classic()+
      theme(text = element_text(size=15))+
      ggtitle("Control vs. Test: Difference (After-Before) comparison")+
      ylim(-0.5,0.5))
  
  (plotControlTestBeforeBefore = CategorizationBeforeAfterDifferenceControlvsTest_means %>% 
      group_by(ExpType,ChosenStep) %>% 
      summarise(meanBefore=mean(Before,na.rm=T)) %>% 
      ggplot(aes(x=ChosenStep,y=meanBefore,group=ExpType,colour=ExpType))+
      geom_rect(xmin=analysisWindowPoints[1],xmax=analysisWindowPoints[3],ymin=0.05,ymax=0.95, colour="grey60", fill="grey100", alpha=0) +
      geom_point()+
      #geom_point(size=2)+
      geom_line()+
      theme_classic()+
      theme(text = element_text(size=15))+
      ylab("Proportion of alveolar responses")+
      ggtitle("Control-Before vs. Test-Before comparison"))
  
  (plotControlTestAfterAfter = CategorizationBeforeAfterDifferenceControlvsTest_means %>% 
      group_by(ExpType,ChosenStep) %>% 
      summarise(meanAfter=mean(After,na.rm=T)) %>% 
      ggplot(aes(x=ChosenStep,y=meanAfter,group=ExpType,colour=ExpType))+
      geom_rect(xmin=analysisWindowPoints[1],xmax=analysisWindowPoints[3],ymin=0.05,ymax=0.95, colour="grey60", fill="grey100", alpha=0) +
      geom_point()+
      #geom_point(size=2)+
      geom_line()+
      theme_classic()+
      theme(text = element_text(size=15))+
      ylab("Proportion of alveolar responses")+
      ggtitle("Control-After vs. Test-After comparison"))
  
  
  (plotsAll = grid.arrange(plotControl,plotTest,
                           plotControlTestBeforeBefore,plotControlTestAfterAfter,
                           plotControlTestDifferences,
                           ncol=2))
  #Saving plot
  ggsave(paste(outputFilename,".eps",sep=""),plotsAll,dpi=200,width=12,height=12,units="in")
  
  
  return(list(LDT=LDT,
              analysisWindowPoints=analysisWindowPoints,
              parameter_m0_BeforeAfter_Control=parameter_m0_BeforeAfter_Control,
              parameter_m1_BeforeAfter_Control=parameter_m1_BeforeAfter_Control,
              anova_BeforeAfter_Control=anova_BeforeAfter_Control,
              parameter_m0_BeforeAfter_Test=parameter_m0_BeforeAfter_Test,
              parameter_m1_BeforeAfter_Test=parameter_m1_BeforeAfter_Test,
              anova_BeforeAfter_Test=anova_BeforeAfter_Test,
              parameter_m0_TestControl_BeforeBefore=parameter_m0_TestControl_BeforeBefore,
              parameter_m1_TestControl_BeforeBefore=parameter_m1_TestControl_BeforeBefore,
              anova_TestControl_BeforeBefore=anova_TestControl_BeforeBefore,
              parameter_m0_TestControl_AfterAfter=parameter_m0_TestControl_AfterAfter,
              parameter_m1_TestControl_AfterAfter=parameter_m1_TestControl_AfterAfter,
              anova_TestControl_AfterAfter=anova_TestControl_AfterAfter,
              parameter_m0_Full=parameter_m0_Full,
              parameter_m1_Full=parameter_m1_Full,
              parameter_m2_Full=parameter_m2_Full,
              parameter_m3_Full=parameter_m3_Full,
              parameter_m4_Full=parameter_m4_Full,
              anova_Test_Full=anova_Test_Full))
}


#glmer tests
#Experiment 1  
GeneralisationMSAnalysis_glmer(test=Exp1Test_Directory,control=Exp1Control_Directory,outputFilename="Experiment1")

#Experiment 2
GeneralisationMSAnalysis_glmer(test=Exp2Test_Directory,control=Exp2Control_Directory,outputFilename="Experiment2")

#Experiment 3
GeneralisationMSAnalysis_glmer(test=Exp3Test_Directory,control=Exp3Control_Directory,outputFilename="Experiment3")


#**************
#Comparing the difference between before-after in experiment 2/3 directly.
testDataOriginal_Exp2 = dataMunger(experiment=Exp2Test_Directory,ExpType="Test")$Categorization %>% 
  mutate(Experiment = "Experiment2") %>% 
  mutate(participant = as.numeric(participant)+10000)
controlDataOriginal_Exp2 = dataMunger(experiment=Exp2Control_Directory,ExpType="Control")$Categorization %>% 
  mutate(Experiment = "Experiment2",
         SubNum = SubNum + max(unique(testDataOriginal_Exp2$SubNum))) %>% 
  mutate(participant = as.numeric(participant)+20000)
testDataOriginal_Exp3 = dataMunger(experiment=Exp3Test_Directory,ExpType="Test")$Categorization %>% 
  mutate(Experiment = "Experiment3",
         SubNum = SubNum + max(unique(controlDataOriginal_Exp2$SubNum))) %>% 
  mutate(participant = as.numeric(participant)+30000)
controlDataOriginal_Exp3 = dataMunger(experiment=Exp3Control_Directory,ExpType="Control")$Categorization %>% 
  mutate(Experiment = "Experiment3",
         SubNum = SubNum + max(unique(testDataOriginal_Exp3$SubNum))) %>% 
  mutate(participant = as.numeric(participant)+40000)

testData_Exp2_Exp3 = rbind(testDataOriginal_Exp2,controlDataOriginal_Exp2,testDataOriginal_Exp3,controlDataOriginal_Exp3) %>%
  group_by(Experiment,SubNum,Type,ExpType,ChosenStep) %>% 
  summarise(sumResponse=sum(Response,na.rm=T)) %>% 
  filter(ChosenStep %in% c(15,17,19)) %>% 
  group_by(Experiment,SubNum,Type,ExpType) %>% 
  summarise(totalSumResponse=sum(sumResponse,na.rm=T)) %>% 
  pivot_wider(names_from=Type,values_from=totalSumResponse)
View(testData_Exp2_Exp3)

m0 = glm(data=testData_Exp2_Exp3,cbind(After,Before)~1,family="binomial")
m1 = glm(data=testData_Exp2_Exp3,cbind(After,Before)~Experiment,family="binomial")
m2 = glm(data=testData_Exp2_Exp3,cbind(After,Before)~ExpType,family="binomial")
m3 = glm(data=testData_Exp2_Exp3,cbind(After,Before)~Experiment+ExpType,family="binomial")
m4 = glm(data=testData_Exp2_Exp3,cbind(After,Before)~Experiment*ExpType,family="binomial")
anova(m0,m1,m2,m3,m4)
summary(m4)


testData_Exp2_Exp3 = rbind(testDataOriginal_Exp2,controlDataOriginal_Exp2,testDataOriginal_Exp3,controlDataOriginal_Exp3) %>% 
  mutate(Type = fct_relevel(Type, "Before"))
m0 = glmer(data=testData_Exp2_Exp3,Response~1 + (Type|SubNum) + (1|ChosenStep),family="binomial")
m9 = glmer(data=testData_Exp2_Exp3,Response~Type*ExpType*Experiment + (Experiment+ExpType|SubNum) + (1|ChosenStep),family="binomial")
summary(m9)

m1_Full = brm(data=testData_Exp2_Exp3,Response~ExpType*Type*Experiment + (1|SubNum) + (1|ChosenStep), family=binomial())
(parameter_m1_Full = data.frame(parameters(m1_Full,centrality="mean")))

#######################################
#brms
GeneralisationMSAnalysis_brms = function(test=Exp2Test_Directory,control=Exp2Control_Directory,outputFilename="Experiment2"){
  
  analysisWindowPoints = c(15,17,19)
  
  #Reading in data
  testDataOriginal = dataMunger(experiment=test,ExpType="Test")
  controlDataOriginal = dataMunger(experiment=control,ExpType="Control")
  #test=controlDataOriginal$LDT
  
  #Categorization data
  (LDT = rbind(testDataOriginal$goodSubjects,controlDataOriginal$goodSubjects) %>% 
      group_by(ExpType) %>% 
      summarise(NumSubjects = length(SubNum),
                # `?` = mean(`?`,na.rm=T),
                None = mean(None,na.rm=T),
                s = mean(s,na.rm=T),
                `f/?` = mean(`f/?`,na.rm=T),
                Overall = mean(Overall,na.rm=T)))
  
  #Categorization data
  testCategorization = testDataOriginal$Categorization
  sort(names(testCategorization))
  controlCategorization = controlDataOriginal$Categorization
  setdiff(names(testCategorization),names(controlCategorization))
  
  #Concatenating the data
  Categorization = rbind(testCategorization,controlCategorization) %>% 
    select(SubNum,Type,ExpType,ChosenStep,Response) %>% 
    mutate(RowNum=row_number()) %>% 
    filter(ChosenStep %in% analysisWindowPoints)
  
  #brms - Comparing before vs. after - control
  CategorizationBeforeAfter = Categorization %>% filter(ExpType=="Control")
  # m0 = brm(data=CategorizationBeforeAfter,Response~1 + (Type|SubNum) + (Type|ChosenStep), family="binomial")
  m1_Control = brm(data=CategorizationBeforeAfter,Response~Type + (Type|SubNum) + (Type|ChosenStep), family=binomial(), control = list(adapt_delta = 0.85))
  (parameter_m1_BeforeAfter_Control = data.frame(parameters(m1_Control,centrality="mean")))
  
  #brms - Comparing Before vs. after - Test
  CategorizationBeforeAfter = Categorization %>% filter(ExpType=="Test")
  # m0 = glmer(data=CategorizationBeforeAfter,Response~1 + (Type|SubNum) + (Type|ChosenStep), family="binomial")
  m1_Test = brm(data=CategorizationBeforeAfter,Response~Type + (Type|SubNum) + (Type|ChosenStep), family=binomial())
  (parameter_m1_BeforeAfter_Test = data.frame(parameters(m1_Test,centrality="mean")))
  
  #brms - Comparing Test vs. Control
  CategorizationTestControl = Categorization %>% filter(Type=="After")
  # m0 = glmer(data=CategorizationTestControl,Response~1 + (ExpType|SubNum) + (ExpType|ChosenStep), family="binomial")
  m1_AfterAfter = brm(data=CategorizationTestControl,Response~ExpType + (ExpType|SubNum) + (ExpType|ChosenStep), family=binomial())
  (parameter_m1_TestControl_AfterAfter = data.frame(parameters(m1_AfterAfter,centrality="mean")))
  
  #brms - Comparing Test vs. Control
  # m0 = glmer(data=CategorizationTestControl,Response~1 + (ExpType|SubNum) + (ExpType|ChosenStep), family="binomial")
  m1_Full = brm(data=Categorization,Response~ExpType*Type + (ExpType|SubNum) + (ExpType|ChosenStep), family=binomial())
  (parameter_m1_Full = data.frame(parameters(m1_Full,centrality="mean")))
  
  return(list(LDT=LDT,
              analysisWindowPoints=analysisWindowPoints,
              parameter_m1_BeforeAfter_Control=parameter_m1_BeforeAfter_Control,
              parameter_m1_BeforeAfter_Test=parameter_m1_BeforeAfter_Test,
              parameter_m1_TestControl_AfterAfter=parameter_m1_TestControl_AfterAfter,
              parameter_m1_Full=parameter_m1_Full))
}

#brms tests
Exp2 = GeneralisationMSAnalysis_brms(test=Exp2Test_Directory,control=Exp2Control_Directory,outputFilename="Experiment3")
Exp3 = GeneralisationMSAnalysis_brms(test=Exp3Test_Directory,control=Exp3Control_Directory,outputFilename="Experiment3")
